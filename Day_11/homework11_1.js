const Koa = require('koa')
const Router = require('koa-router')
const render = require('koa-ejs')
const path = require('path')
const app = new Koa()
const router = new Router()

const mysql = require('mysql2/promise')
const pool = mysql.createPool({
    connectlimit : 10,
    host : 'localhost',
    user : 'root',
    password : '',
    database : 'codecamp2'
})

async function myQuery() {
    try {
        let [rows, fields] = await pool.query('SELECT * FROM user');
        return rows;
    } catch(err){
        console.error(err)
    }
}

render(app, {
    root:path.join(__dirname, 'views'),
    layout:'template',
    viewExt:'ejs',
    cache:false
})
router.get('/', async ctx =>{
    let userData = {}
    userData.TextRow = await myQuery()
    await ctx.render('homework11_1', userData)
})

app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)