fetch('../Day2/homework2_1.json').then(response => {
    return response.json();
})
.then(employees => {
    function addYearSalary(row) {
        row.YearSalary = row.Salary * 12;
    }
    function addNextSalary(row){
        row.NextSalary = [];
        currentSalary = row.Salary;
        row['NextSalary'].push(currentSalary);
        for(let year=1; year<3; year++){
            currentSalary = (currentSalary*110)/100;
            row['NextSalary'].push(currentSalary);
        }
    }
    let newEmployees = addAdditionalFields(employees); //แก้ไข Function addAdditionalFields()
    function addAdditionalFields() {
        let item = "";
        let header = "";
        let nextSalary = "";
        for(let person in employees ) {
            addYearSalary(employees[person]);
            addNextSalary(employees[person]);
            if (person == 0) {
                for (let y in employees[person]) {
                    header += "<th>" + y + "</th>";
                }
                header += "</tr>"            
                let numlist = 0;
                for (let list in employees[person]) {
                    if(numlist==6){
                        item += "<td>"
                        nextSalary = "<ol>"
                        for(let z in employees[person][list]){
                            item += "<li>" + employees[person][list][z] + "</li>";
                        }
                        item += nextSalary += "</ol></td>"
                    } else {
                        item += "<td>" + employees[person][list] + "</td>";
                    }
                    numlist++;
                }
                item += "</tr>"
            } else {
                let numlist = 0;
                for (let list in employees[person]) {
                    if(numlist==6){
                        item += "<td>"
                        nextSalary = "<ol>"
                        for(let z in employees[person][list]){
                            item += "<li>" + employees[person][list][z] + "</li>";
                        }
                        item += nextSalary += "</ol></td>"
                    } else {
                        item += "<td>" + employees[person][list] + "</td>";
                    }
                    numlist++;
                }
                item += "</tr>"
            }
            document.getElementById("myTable").innerHTML = header+item;
        }
    }
})
.catch(error => {
    console.error('Error:', error);
});