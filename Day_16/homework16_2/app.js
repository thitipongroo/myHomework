const Koa = require('koa')
const Router = require('koa-router')
const path = require('path')
const render = require('koa-ejs')
const mysql2 = require('mysql2/promise')

const app = new Koa()
const router = new Router()

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})

const pool  = mysql2.createPool({
   connectionLimit : 10,
   host            : 'localhost',
   user            : 'root',
   password        : '',
   database        : 'design_pattern'
});

const istModel = require('./model/instructor');
const istCon = require('./controllers/instructor');
const istGetData = istCon(pool, istModel);

const courseModel = require('./model/course');
const courseCon = require('./controllers/course');
const courseGetData = courseCon(pool, courseModel);

router.get('/instructors/', istGetData.findInstructors);
router.get('/courses/', courseGetData.findCourses);

app.use(router.routes())
app.use(router.allowedMethods())

app.listen(3000)