module.exports = function(pool, istModel) {
    return {
        async findInstructors(ctx, next) {
            const instructorObject = await istModel.findAll(pool);
             await ctx.render('table', {
                information: instructorObject
            });
        }
    }
}