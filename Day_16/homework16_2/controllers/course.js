module.exports = function (pool, courseModel) {
    return {
        async findCourses(ctx, next) {
            const courseObject = await courseModel.findAll(pool);
             await ctx.render('table', {
                information : courseObject
            });
        }
    }
}