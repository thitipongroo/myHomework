module.exports = {
    createEntity(row) {
        if (!row.id)
            return [];
         
        return {
            id: row.id,
            name: row.name,
            price: row.price,
            created_at: row.created_at,
        }
    },
    async findAll(pool) {
        const [rows] = await pool.query('SELECT * FROM courses');
        return rows.map(this.createEntity);
    }
}