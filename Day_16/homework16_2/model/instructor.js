module.exports = {
    createEntity(row) {
        if (!row.id)
            return [];
         
        return {
            id: row.id,
            name: row.name,
            created_at: row.created_at,
        }
    },
    async findAll(pool) {
        const [rows] = await pool.query('SELECT * FROM instructors');
        return rows.map(this.createEntity);
    }
}