const Koa = require('koa')
const Router = require('koa-router')
const render = require('koa-ejs')
const serv = require('koa-static')
const path = require('path')
const koaBody = require('koa-body')

const app = new Koa()
const router = new Router()

render(app, {
    root: path.join(__dirname, 'src/views'),
    layout : 'template',
    viewExt: 'ejs',
    cache: false
})

async function signinGetHandler (ctx) {
    await ctx.render('signin')
}

async function signupGetHandler (ctx) {
    await ctx.render('signup')
}

async function uploadGetHandler (ctx) {
    await ctx.render('upload')
}

async function profileGetHandler (ctx) {
    await ctx.render('profile')
}

router.get('/', signinGetHandler)
router.get('/signin', signinGetHandler)
router.get('/signup', signupGetHandler)
router.get('/upload', uploadGetHandler)
router.get('/profile', profileGetHandler)

app.use(serv(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())
app.listen(8000)