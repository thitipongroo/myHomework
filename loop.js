function draw1(n){
    let stars = ''
    for(let i=1; i<=n; i++){
        stars += '*'
    }
    console.log('ข้อ 1.\n'+stars+'\n')
}

function draw2(n){
    let stars = ''
    for(let i=1; i<=n; i++){
        for(let j=1; j<=n; j++){
            stars += '*'
        }
        stars += '\n'
    }
    console.log('ข้อ 2.\n'+stars+'\n')
}

function draw3(n){
    let number = ''
    for(let i=1; i<=n; i++){
        for(let j=1; j<=n; j++){
            number += j
        }
        number += '\n'
    }
    console.log('ข้อ 3.\n'+number+'\n')
}

function draw4(n){
    let number = ''
    for(let i=1; i<=n; i++){
        for(let j=1; j<=n; j++){
            number = number + i
        }
        number = number + '\n'
    }
    console.log('ข้อ 4.\n'+number+'\n')
}

function draw5(n){
    let number = ''
    for(let i=n; i>=1; i--){
        for(let j=1; j<=n; j++){
            number += i
        }
        number += '\n'
    }
    console.log('ข้อ 5.\n'+number+'\n')
}

function draw6(n){
    let number = ''
    for(let i=1; i<=n*n; i++){
        number += i
        if(i%n==0){
            number += '\n'
        }
    }
    console.log('ข้อ 6.\n'+number+'\n')
}

function draw7(n){
    let number = ''
    for(let i=n*n; i>=1; i--){
        number += i
        if(i%n==1){
            number += '\n'
        }
    }
    console.log('ข้อ 7.\n'+number+'\n')
}

function draw8(n){
    let number = ''
    for(let i=0; i<=n-1; i++){
        number += i*2 + '\n'
    }
    console.log('ข้อ 8.\n'+number+'\n')
}

function draw9(n){
    let number = ''
    for(let i=1; i<=n; i++){
        number += i*2 + '\n'
    }
    console.log('ข้อ 9.\n'+number+'\n')
}

function draw10(n){
    let number = ''
    for(let i=1; i<=n; i++){
        for(let j=1; j<=n; j++){
            number += i*j
        }
        number += '\n'
    }
    console.log('ข้อ 10.\n'+number+'\n')
}

function draw11(n){
    let stars = ''
    for(let i=1; i<=n; i++){
        for(let j=1; j<=n; j++){
            if(i==j){
                stars += '_'
            } else {                
                stars += '*'
            }
        }
        stars += '\n'
    }
    console.log('ข้อ 11.\n'+stars+'\n')
}

function draw12(n){
    let stars = ''
    for(let i=0; i<=n-1; i++){
        for(let j=1; j<=n; j++){
            if(j == n-i){
                stars += '_'
            } else {                
                stars += '*'
            }
        }
        stars += '\n'
    }
    console.log('ข้อ 12.\n'+stars+'\n')
}

function draw13(n){
    let stars = ''
    for(let i=1; i<=n; i++){
        for(let j=1; j<=n; j++){
            if(j>i){
                stars += '_'
            } else {                
                stars += '*'
            }
        }
        stars += '\n'
    }
    console.log('ข้อ 13.\n'+stars+'\n')
}

function draw14(n){
    let stars = ''
    for(let i=1; i<=n; i++){
        for(let j=1; j<=n; j++){
            if(i+j <= n+1){
                stars += '*'
            } else {                
                stars += '_'
            }
        }
        stars += '\n'
    }
    console.log('ข้อ 14.\n'+stars+'\n')
}

function draw15(n){
    let stars = ''
    let k = 1
    for(let i=1; i<=(2*n-1); i++){
        if(i<=n){
            for(let j=1; j<=n; j++){
                if(j>i){
                    stars += '_'
                } else {                
                    stars += '*'
                }
            }
            stars += '\n'
        } else {
            for(let j=1; j<=n; j++){
                if(j+k < n+1){
                    stars += '*'
                } else {                
                    stars += '_'
                }
            }
            stars += '\n'
            k++
        }   
    }
    console.log('ข้อ 15.\n'+stars+'\n')
}

function draw16(n){
    let number = ''
    let k = n-1
    for(let i=1; i<=(2*n-1); i++){
        if(i<=n){
            for(let j=1; j<=n; j++){
                if(j>i){
                    number += '_'
                } else {                
                    number += i
                }
            }
            number += '\n'
        } else {
            for(let j=1; j<=n; j++){
                if(j<=k){
                    number += k
                } else {                
                    number += '_'
                }
            }
            number += '\n'
            k--
        }   
    }
    console.log('ข้อ 16.\n'+number+'\n')
}

function draw17(n){
    let stars = ''
    for(let i=1; i<=n; i++){
        for(let j=1; j<=n; j++){
            if(i+j >= n+1){
                stars += '*'
            } else {                
                stars += '_'
            }
        }
        stars += '\n'
    }
    console.log('ข้อ 17.\n'+stars+'\n')
}

function draw18(n){
    let stars = ''
    for(let i=1; i<=n; i++){
        for(let j=1; j<=n; j++){
            if(i>j){
                stars += '_'
            } else {                
                stars += '*'
            }
        }
        stars += '\n'
    }
    console.log('ข้อ 18.\n'+stars+'\n')
}

function draw19(n){
    let stars = ''
    let k = 1
    for(let i=1; i<=(2*n-1); i++){
        if(i<=n){
            for(let j=1; j<=n; j++){
                if(i+j <= n){
                    stars += '_'
                } else {                
                    stars += '*'
                }
            }
            stars += '\n'
        } else {
            for(let j=1; j<=n; j++){
                if(j<=k){
                    stars += '_'
                } else {                
                    stars += '*'
                }
            }
            stars += '\n'
            k++
        }   
    }
    console.log('ข้อ 19.\n'+stars+'\n')
}

function draw20(n) {
    let number = ''
    let k = 0
    for(let i=1; i<=(2*n-1); i++){
        let diff = Math.abs(n-i)
        for(let j=1; j<=n; j++){
            if(j<=diff){
                number += '-'
            } else {
                number += ++k
            }
        }
        number += '\n'
    }
    console.log('ข้อ 20.\n'+number+'\n')
}

function draw21(n) {
    let stars = ''
    for(let i=1; i<=n; i++){
        for(let j=1; j<=(2*n-1); j++){
            if(j>(2*n)-(n-i+1) || j<(n-i+1)){
                stars += '-'
            } else {
                stars += '*'
            }
        }
        stars += '\n'
    }
    console.log('ข้อ 21.\n'+stars+'\n')
}

function draw22(n) {
    let stars = ''
    for(let i=1; i<=n; i++){
        for(let j=1; j<=(2*n-1); j++){
            if(j>(2*n-i) || j<i){
                stars += '-'
            } else {
                stars += '*'
            }
        }
        stars += '\n'
    }
    console.log('ข้อ 22.\n'+stars+'\n')
}

function draw23(n) {
    let stars = ''
    for(let i=1; i<=(2*n-1); i++){
        let diff = Math.abs(n-i)
        for(let j=1; j<=(2*n-1); j++){
            if(j>=(n+n-diff) || j<=diff){
                stars += '-'
            } else {
                stars += '*'
            }
        }
        stars += '\n'
    }
    console.log('ข้อ 23.\n'+stars+'\n')
}

function draw24(n) {
    let number = ''
    let k = 0
    for(let i=1; i<=(2*n-1); i++){
        let diff = Math.abs(n-i)
        for(let j=1; j<=(2*n-1); j++){
            if(j>=(n+n-diff) || j<=diff){
                number += '-'
            } else {
                number += ++k
            }
        }
        number += '\n'
    }
    console.log('ข้อ 24.\n'+number+'\n')
}

draw1(5)
draw2(5)
draw3(5)
draw4(5)
draw5(5)
draw6(5)
draw7(5)
draw8(5)
draw9(5)
draw10(5)
draw11(5)
draw12(5)
draw13(5)
draw14(5)
draw15(5)
draw16(5)
draw17(5)
draw18(5)
draw19(5)
draw20(5)
draw21(5)
draw22(5)
draw23(5)
draw24(5)