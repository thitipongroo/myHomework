const Koa = require('koa')
const Router = require('koa-router')
const render = require('koa-ejs')
const path = require('path')
const app = new Koa()
const router = new Router()

const mysql = require('mysql2/promise')
const pool = mysql.createPool({
    connectlimit : 10,
    host : 'localhost',
    user : 'root',
    password : '',
    database : 'codecamp2'
})

async function myQueryCourse(query) {
    try {
        let [rows] = await pool.query(query);
        return rows;
    } catch(err){
        console.error(err)
    }
}

async function myQueryStudent(query) {
    try {
        let [rows] = await pool.query(query);
        return rows;
    } catch(err){
        console.error(err)
    }
}

render(app, {
    root:path.join(__dirname, 'views'),
    layout:'template',
    viewExt:'ejs',
    cache:false
})

router.get('/', async ctx =>{
    let cmdCourse = 'SELECT c.name, count(c.name) as count_name, c.price FROM courses as c INNER JOIN enrolls as e ON c.id = e.course_id GROUP BY c.id UNION SELECT " ", "รวมทั้งหมด", sum(c.price) FROM courses as c INNER JOIN enrolls as e ON c.id = e.course_id'
    let cmdStudent = 'SELECT s.name, count(s.id) as count_course, c.price, sum(c.price) as sum FROM students s INNER JOIN enrolls e ON s.id = e.student_id INNER JOIN courses c ON c.id = e.course_id GROUP BY s.id'
    let queryData = {}
    queryData.TextRowCourse = await myQueryCourse(cmdCourse)
    queryData.TextRowStudent = await myQueryStudent(cmdStudent)
    await ctx.render('homework14_1', queryData)
})

app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)