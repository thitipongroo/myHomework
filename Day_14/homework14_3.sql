CREATE VIEW enroll_details AS
SELECT s.name AS student_name, c.name AS course_name, c.detail, c.price, COALESCE(i.name, 'No Instructor') AS instructors FROM enrolls e
INNER JOIN students s ON s.id = e.student_id
INNER JOIN courses c ON c.id = e.course_id
LEFT JOIN instructors i ON i.id = c.teach_by
UNION SELECT 'Total',' ', ' ',  sum(price), ' ' FROM courses