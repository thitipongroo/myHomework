-- ข้อ 1
SELECT s.name as student_name, sum(c.price) as value, count(s.id) as course  FROM students s
INNER JOIN enrolls e ON s.id = e.student_id
INNER JOIN courses c ON c.id = e.course_id GROUP BY s.id;

-- ข้อ 2
SELECT mp.student_name, c.name, mp.max_price FROM enrolls e
INNER JOIN courses c ON c.id = e.course_id
INNER JOIN (
SELECT s.id as student_id, s.name as student_name, MAX(c.price) as max_price FROM students s
    INNER JOIN enrolls e ON s.id = e.student_id
    INNER JOIN courses c ON c.id = e.course_id
GROUP BY s.id) mp ON mp.student_id = e.student_id AND mp.max_price = c.price

-- ข้อ 3
SELECT s.name as student_name, c.name as course_name, avg(c.price) FROM students s
INNER JOIN enrolls e ON s.id = e.student_id
INNER JOIN courses c ON c.id = e.course_id GROUP BY s.id;