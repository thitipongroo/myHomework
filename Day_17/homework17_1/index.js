const Koa = require('koa')
const Router = require('koa-router')
const path = require('path')
const render = require('koa-ejs')
const koaBody = require('koa-body')
const bcrypt = require('bcrypt')
const mysql = require('mysql2/promise')

// Instance
const app = new Koa()
const router = new Router()
const pool = mysql.createPool({
    connectionLimit: 10,
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'pikkanode'
})

// Configurarion
render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})

// Router

router.get('/', async (ctx, next) => {
    await ctx.render('signin')
    await next()
})
router.post('/signin_completed', async (ctx, next) => {
    await ctx.render('signin_completed',{
        email : ctx.request.body.email,
        password : ctx.request.body.password
    })
    // ctx.body = 'username : '+ctx.request.body.username+ 'password : '+ctx.request.body.password
    await next()
})

router.get('/signup', async (ctx, next) => {
    await ctx.render('signup')
    await next()
})
router.post('/signup_completed', async (ctx, next) => {
    const aData = [
        ctx.request.body.email,
        await bcrypt.hash(ctx.request.body.password, 10)
    ]
    await pool.query(`INSERT INTO users (email, password) VALUES (?, ?)`, aData)
    await ctx.render('signup_completed')
    await next()
})

// Middleware
app.use(koaBody())
app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)