CREATE DATABASE shop;
use shop;

CREATE TABLE customer (
  id int auto_increment,
  name VARCHAR(60) NOT NULL,
  address VARCHAR(255),
  created_at timestamp not null default now(),
  primary key (id)
);

CREATE TABLE department (
    id INT auto_increment,
    name VARCHAR (60) NOT NULL,
    budget INT,
  created_at timestamp not null default now(),
    PRIMARY KEY (id)
);

CREATE TABLE supplier (
    id INT auto_increment,
    name VARCHAR (60) NOT NULL,
    address VARCHAR (255),
    phone_number VARCHAR (60),
    created_at timestamp not null default now(),
    PRIMARY KEY (id)
);

create TABLE product (
    id int auto_increment,
    name VARCHAR (60) NOT NULL,
    product_desc VARCHAR (255),
    price INT,
    quantity INT,
    supplier_id INT,
    created_at timestamp not null default now(),
    PRIMARY KEY(id),
    FOREIGN KEY (supplier_id) REFERENCES supplier (id)
);

CREATE TABLE employee (
  id int auto_increment,
  name varchar(60) not null,
  address varchar(255),
  salary int,
  teach_by int,
  department_id INT,
  created_at timestamp not null default now(),
  primary key (id),
  foreign key (department_id) references department (id)
);

create table orders (
  id int auto_increment,
  order_date timestamp not null default now(),
  customer_id INT,
  employee_id INT,
  created_at timestamp not null default now(),
  primary key (id),
  FOREIGN KEY (customer_id) REFERENCES customer (id),
  FOREIGN KEY (employee_id) REFERENCES employee (id)
);

create table order_item (
  id int auto_increment,
  order_item INT,
  amount INT not null,
  discount INT,
  created_at timestamp not null default now(),
  primary key (id),
  FOREIGN KEY (order_item) REFERENCES orders (id)
);