let arr = [1,2,3,4,5,6,7,8,9,10];

function modTwo (number){
    return number % 2 === 0;
}

function multiThousand (number){
    return number * 1000;
}

const result = arr
    .filter(modTwo)
    .map(multiThousand);

console.log(result);
