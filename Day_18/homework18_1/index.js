const Koa = require('koa')
const Router = require('koa-router')
const path = require('path')
const render = require('koa-ejs')
const koaBody = require('koa-body')
const bcrypt = require('bcrypt')
const mysql = require('mysql2/promise')
const session = require('koa-session')

// Instance
const app = new Koa()
const router = new Router()
const pool = mysql.createPool({
    connectionLimit: 10,
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'pikkanode'
})
const sessionStore = {}
const sessionConfig = {
    key : 'sess',
    maxAge : 1000*60*60,
    httpOnly : true,
    store : {
        get (key, maxAge, {rolling}){
            return sessionStore[key]
        },
        set (key, sess, maxAge, {rolling}){
            sessionStore[key] = sess
        },
        destroy (key) {
            delete sessionStore[key]
        }
    }
}
app.key = ['supersecret']

// Configurarion
render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})

// Function
// async function signingetHandler (ctx) {
//     const data = {
//         flash : ctx.flash
//     }
//     await ctx.render('signin', data)
// }

function signinPostHandler (ctx) {
    if(!ctx.request.body.email){
        ctx.flash = { error : 'E-mail is required'}
        return ctx.redirect('/signin')
    } else if(!ctx.request.body.password){
        ctx.flash = { error : 'Password is required'}
    }
    ctx.body = 'Signin Completed'
}

// Router
router.get('/', async (ctx, next) => {
    await ctx.render('signin')
    await next()
})
router.post('/signin', signinPostHandler)

router.get('/signup', async (ctx, next) => {
    await ctx.render('signup')
    await next()
})
router.post('/signup', async (ctx, next) => {
    const aData = [
        ctx.request.body.email,
        await bcrypt.hash(ctx.request.body.password, 10)
    ]
    await pool.query(`INSERT INTO users (email, password) VALUES (?, ?)`, aData)
    await ctx.render('signin')
    await next()
})

// Middleware
app.use(session(sessionConfig, app))
// app.use(flash)
app.use(koaBody())
app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)