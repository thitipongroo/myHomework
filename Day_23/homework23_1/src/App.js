import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Hello from './Table';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <table className="table tableStriped">
          <thead>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            <th scope="col">Last</th>
            <th scope="col">Handler</th>
          </thead>
          <Hello/>
        </table>
      </div>
    );
  }
}

export default App;