import React, {Component} from 'react';
const people = [
    {
        id:1,
        first:'Mark',
        last:'Otto',
        handle:'@mdo'
    },    
    {
        id:2,
        first:'Jacob',
        last:'Thornton',
        handle:'@fat'
    },
    {
        id:3,
        first:'Larry',
        last:'The bird',
        handle:'@twitter'
    }
]
class Hello extends Component{
    render(){
        const msgs = people.map(person =>
            <tr><th scope="row" key={person.id}>{person.id}</th><td key={person.id}>{person.first}</td><td key={person.id}>{person.last}</td><td key={person.id}>{person.handle}</td></tr>
        )
        return <tbody>{msgs}</tbody>
    }
}

export default Hello