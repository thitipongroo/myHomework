const arr = [3,44,38,5,47,15,36,27,2,46,4,19,50,48]

function swap(myArray, index, index2) {
    let temp;
    temp = myArray[index];
    myArray[index] = myArray[index2];
    myArray[index2] = temp;
}

function bubbleSort (myArr) {
    let swapped
    let sorted = myArr.length-1
    do{
        swapped = false
        for(let i=0; i<=sorted; i++){
            if(myArr[i] > myArr[i+1]){
                swap(myArr,i,i+1)
                swapped = true
            }
        }
        console.log(JSON.stringify(arr))
        sorted--
    } 
    while(swapped)
}

bubbleSort(arr)
// console.log(JSON.stringify(arr))