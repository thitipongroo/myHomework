const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')

const app = new Koa()
const router = new Router()

render(app,{
    root:path.join(__dirname, 'views'),
    layout:'template',
    viewExt:'ejs',
    cache:false
})
router.get('/', ctx => {
    ctx.body = "This is index page"
})

router.get('/about', ctx => {
    ctx.body = "This is about page"
})
router.get('/image', ctx => {
    ctx.body = '<img src="images/picture.jpg" width="1920px" height="1080px">'
})
router.get('/table', async ctx => {
    await ctx.render('landing')
})

app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)