const Koa = require('koa')
const Router = require('koa-router')
const render = require('koa-ejs')
const path = require('path')
const app = new Koa()
const router = new Router()

const mysql = require('mysql2/promise')
const pool = mysql.createPool({
    connectlimit:10,
    host:'localhost',
    user:'root',
    password:'',
    database:'codecamp2'
})

async function myQueryInstructor(query) {
    try {
        let [rows] = await pool.query(query);
        return rows;
    } catch(err){
        console.error(err)
    }
}

async function myQueryCourse(query) {
    try {
        let [rows] = await pool.query(query);
        return rows;
    } catch(err){
        console.error(err)
    }
}

render(app, {
    root:path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})

router.get('/', async ctx =>{
    let cmdTeach = 'SELECT i.id, i.name as teach_name, c.name as course_name FROM instructors i LEFT JOIN courses c ON i.id = c.teach_by WHERE c.teach_by IS NULL ORDER BY i.id'
    let cmdCourse = 'SELECT c.id, c.name as course_name, c.price, i.name as instructor_name FROM courses c LEFT JOIN instructors i ON c.teach_by = i.id WHERE c.teach_by IS NULL ORDER BY c.id'
    let queryData = {}
    queryData.TextRowStaf = await myQueryInstructor(cmdTeach)
    queryData.TextRowBook = await myQueryCourse(cmdCourse)
    await ctx.render('homework13_1', queryData)
})

app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)