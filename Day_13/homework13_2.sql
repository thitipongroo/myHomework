INSERT INTO students (name) VALUES ('Yorn'),('Valhein'),('Zanis'),('Lubu'),('Taara'),('Butterfly'),('Veera'),('Gildur'),('Aleister'),('Skud');
INSERT INTO enrolls (student_id,course_id) VALUES (1,1),(2,1),(3,3),(4,4),(5,5),(6,6),(7,7),(8,8),(9,9),(10,11);
SELECT c.name FROM courses c INNER JOIN enrolls e ON c.id = e.course_id GROUP BY c.id;
SELECT c.name FROM courses c LEFT JOIN enrolls e ON c.id = e.course_id WHERE e.course_id IS NULL;