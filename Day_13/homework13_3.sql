-- ข้อ 1
SELECT c.name as course_name, i.name as teach_name, c.price FROM courses c 
INNER JOIN enrolls e ON c.id = e.course_id
LEFT JOIN instructors i ON c.teach_by = i.id
GROUP BY c.id;

-- ข้อ 2
SELECT c.id, c.name as course_name, i.name as teach_name, c.price FROM courses c 
LEFT JOIN enrolls e ON c.id = e.course_id
LEFT JOIN instructors i ON c.teach_by = i.id
WHERE e.course_id IS NULL
GROUP BY c.id;

-- ข้อ 3
SELECT c.id, c.name as course_name, i.name as teach_name, c.price FROM courses c 
LEFT JOIN enrolls e ON c.id = e.course_id 
LEFT JOIN instructors i ON c.teach_by = i.id
WHERE e.course_id IS NULL AND i.id IS NOT NULL
GROUP BY c.id;