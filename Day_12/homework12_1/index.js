const Koa = require('koa')
const Router = require('koa-router')
const render = require('koa-ejs')
const path = require('path')
const app = new Koa()
const router = new Router()

const mysql = require('mysql2/promise')
const pool = mysql.createPool({
    connectlimit : 10,
    host : 'localhost',
    user : 'root',
    password : '',
    database : 'db_seed'
})

async function myQueryStaf(query) {
    try {
        let [rows] = await pool.query(query);
        return rows;
    } catch(err){
        console.error(err)
    }
}

async function myQueryBook(query) {
    try {
        let [rows] = await pool.query(query);
        return rows;
    } catch(err){
        console.error(err)
    }
}

render(app, {
    root:path.join(__dirname, 'views'),
    layout:'template',
    viewExt:'ejs',
    cache:false
})
router.get('/', async ctx =>{
    let queryStaf = 'SELECT * FROM staffs'
    let queryBook= 'SELECT * FROM books'
    let queryData = {}
    queryData.TextRowStaf = await myQueryStaf(queryStaf)
    queryData.TextRowBook = await myQueryStaf(queryBook)
    await ctx.render('homework12_1', queryData)
})

app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)