let peopleSalary = [
    {"id": "1001", "firstname": "Luke", "lastname": "Skywalker", "salary": ["40000","44000","48400"]},
    {"id": "1002", "firstname": "Tony", "lastname": "Stark", "salary": ["1000000","1100000","1210000"]},
    {"id": "1003", "firstname": "Somchai", "lastname": "Jaidee", "salary": ["20000","22000","24200"]},
    {"id": "1004", "firstname": "Monkey D", "lastname": "Luffee", "salary": ["9000000","9900000","10890000"]}
]
// Variable
let tbHead = ""; //ข้อมูลหัวตาราง
let tbItems = ""; //ข้อมูลรายละเอียด
let moneyUplevel = ""; //ข้อมูลปรับเงินเดือน

for (let x in peopleSalary) {
    if (x == 0) {
        for (let y in peopleSalary[x]) {
            tbHead += "<th>" + y + "</th>";
        }
        tbHead += "</tr>"
        let countObj = 0; //จำนวนข้อมูลปรับเงินเดือน
        for (let y in peopleSalary[x]) {
            if(countObj == 3){
                tbItems += "<td>"
                moneyUplevel = "<ol>"
                for(let z in peopleSalary[x][y]){
                    moneyUplevel += "<li>" + peopleSalary[x][y][z] + "</li>";
                }
                tbItems += moneyUplevel += "</ol></td>"
            } else {
                tbItems += "<td>" + peopleSalary[x][y] + "</td>";
            }
            countObj++;
        }
        tbItems += "</tr>"
    } else {
        let countObj = 0;
        for (let y in peopleSalary[x]) {
            if(countObj == 3){
                tbItems += "<td>"
                moneyUplevel = "<ol>"
                for(let z in peopleSalary[x][y]){
                    moneyUplevel += "<li>" + peopleSalary[x][y][z] + "</li>";
                }
                tbItems += moneyUplevel += "</ol></td>"
            } else {
                tbItems += "<td>" + peopleSalary[x][y] + "</td>";
            }
            countObj++;
        }
        tbItems += "</tr>"
    }
    document.getElementById("myTable").innerHTML = tbHead + tbItems;
}