'use strict'
let fs = require('fs')

function readRobotxt(robotFile){
    return new Promise(function(resolve, reject) {
        fs.readFile('..\\myHomework\\Day_09\\'+robotFile, 'utf8', function(err,data) {
            if (err)
                reject(err);
            else
                resolve(data);
        });
    });    
}

function writeRobotxt(robotxt){
    return new Promise(function(resolve, reject) {
        fs.writeFile('..\\myHomework\\Day_09\\homework9_1\\robot.txt', robotxt, 'utf8', function(err,data) {
            if (err)
                reject(err);
            else
                resolve('Write Success!');
        });
    });
}

async function replaceRobotxt() {
    try{
        let getTxt = ''
        getTxt += await readRobotxt('head.txt') + '\n';
        getTxt += await readRobotxt('body.txt') + '\n';
        getTxt += await readRobotxt('leg.txt') + '\n';
        getTxt += await readRobotxt('feet.txt') + '\n';
        await writeRobotxt(getTxt);
    } catch(err){
        console.error(err);
    }
}

replaceRobotxt()